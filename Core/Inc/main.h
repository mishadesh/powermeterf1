/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f1xx_ll_adc.h"
#include "stm32f1xx_ll_dma.h"
#include "stm32f1xx_ll_i2c.h"
#include "stm32f1xx_ll_rcc.h"
#include "stm32f1xx_ll_bus.h"
#include "stm32f1xx_ll_system.h"
#include "stm32f1xx_ll_exti.h"
#include "stm32f1xx_ll_cortex.h"
#include "stm32f1xx_ll_utils.h"
#include "stm32f1xx_ll_pwr.h"
#include "stm32f1xx_ll_spi.h"
#include "stm32f1xx_ll_usart.h"
#include "stm32f1xx.h"
#include "stm32f1xx_ll_gpio.h"

#if defined(USE_FULL_ASSERT)
#include "stm32_assert.h"
#endif /* USE_FULL_ASSERT */

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define LED_Pin LL_GPIO_PIN_13
#define LED_GPIO_Port GPIOC
#define RELAY_1_Pin LL_GPIO_PIN_14
#define RELAY_1_GPIO_Port GPIOC
#define RELAY_2_Pin LL_GPIO_PIN_15
#define RELAY_2_GPIO_Port GPIOC
#define CURRENT_POS_Pin LL_GPIO_PIN_0
#define CURRENT_POS_GPIO_Port GPIOA
#define CURRENT_NEG_Pin LL_GPIO_PIN_1
#define CURRENT_NEG_GPIO_Port GPIOA
#define V_USR_1_Pin LL_GPIO_PIN_2
#define V_USR_1_GPIO_Port GPIOA
#define V_USR_2_Pin LL_GPIO_PIN_3
#define V_USR_2_GPIO_Port GPIOA
#define V_24_Pin LL_GPIO_PIN_4
#define V_24_GPIO_Port GPIOA
#define V_12_Pin LL_GPIO_PIN_5
#define V_12_GPIO_Port GPIOA
#define BRIDGE_1_Pin LL_GPIO_PIN_6
#define BRIDGE_1_GPIO_Port GPIOA
#define BRIDGE_2_Pin LL_GPIO_PIN_7
#define BRIDGE_2_GPIO_Port GPIOA
#define UART_MAIN_DIR_Pin LL_GPIO_PIN_0
#define UART_MAIN_DIR_GPIO_Port GPIOB
#define NRF_IRQ_Pin LL_GPIO_PIN_1
#define NRF_IRQ_GPIO_Port GPIOB
#define UART_MAIN_TX_Pin LL_GPIO_PIN_10
#define UART_MAIN_TX_GPIO_Port GPIOB
#define UART_MAIN_RX_Pin LL_GPIO_PIN_11
#define UART_MAIN_RX_GPIO_Port GPIOB
#define NRF_CS_Pin LL_GPIO_PIN_12
#define NRF_CS_GPIO_Port GPIOB
#define NRF_SCK_Pin LL_GPIO_PIN_13
#define NRF_SCK_GPIO_Port GPIOB
#define NRF_MISO_Pin LL_GPIO_PIN_14
#define NRF_MISO_GPIO_Port GPIOB
#define NRF_MOSI_Pin LL_GPIO_PIN_15
#define NRF_MOSI_GPIO_Port GPIOB
#define NRF_CE_Pin LL_GPIO_PIN_8
#define NRF_CE_GPIO_Port GPIOA
#define UART_PZEM_TX_Pin LL_GPIO_PIN_9
#define UART_PZEM_TX_GPIO_Port GPIOA
#define UART_PZEM_RX_Pin LL_GPIO_PIN_10
#define UART_PZEM_RX_GPIO_Port GPIOA
#define BMP_SCL_Pin LL_GPIO_PIN_8
#define BMP_SCL_GPIO_Port GPIOB
#define BMP_SDA_Pin LL_GPIO_PIN_9
#define BMP_SDA_GPIO_Port GPIOB
#ifndef NVIC_PRIORITYGROUP_0
#define NVIC_PRIORITYGROUP_0         ((uint32_t)0x00000007) /*!< 0 bit  for pre-emption priority,
                                                                 4 bits for subpriority */
#define NVIC_PRIORITYGROUP_1         ((uint32_t)0x00000006) /*!< 1 bit  for pre-emption priority,
                                                                 3 bits for subpriority */
#define NVIC_PRIORITYGROUP_2         ((uint32_t)0x00000005) /*!< 2 bits for pre-emption priority,
                                                                 2 bits for subpriority */
#define NVIC_PRIORITYGROUP_3         ((uint32_t)0x00000004) /*!< 3 bits for pre-emption priority,
                                                                 1 bit  for subpriority */
#define NVIC_PRIORITYGROUP_4         ((uint32_t)0x00000003) /*!< 4 bits for pre-emption priority,
                                                                 0 bit  for subpriority */
#endif
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
