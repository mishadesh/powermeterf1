/**
  ******************************************************************************
  * File Name          : USART.h
  * Description        : This file provides code for the configuration
  *                      of the USART instances.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __usart_H
#define __usart_H

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include <main.h>
#include "stdbool.h"

 typedef enum
 {
 	UART_STATE_CLOSED 		= 0,
 	UART_STATE_OPENED 		= 1,
 	UART_STATE_INITIALIZED  = 2,
 	UART_STATE_BUSY			= 3
 }teUARTState;

typedef enum
{
	USART1_IFACE = 0,
	USART3_IFACE = 1

}teUSARTIface;

typedef struct
{
	uint8_t* pubDatArray;
	uint8_t* pubDatLength;
	bool*    pbIsNewData;
}tUARTData;

void USART_vInit(teUSARTIface eUART, void* pUART_TxCpltCb, void* pUART_RxCpltCb, uint32_t *ulSpeed, void (*pUART_DirectionPinStateChangeCb)(bool pinstate), tUARTData* pUARTData);
void USART_TransmitData(teUSARTIface eUART ,uint8_t* pData, uint8_t length);

/* USER CODE BEGIN Prototypes */

/* USER CODE END Prototypes */

#ifdef __cplusplus
}
#endif
#endif /*__ usart_H */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
