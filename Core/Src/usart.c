/**
  ******************************************************************************
  * File Name          : USART.c
  * Description        : This file provides code for the configuration
  *                      of the USART instances.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "usart.h"
#include "stm32f1xx_ll_dma.h"
#include "stddef.h"

#define DMA_PRELOAD_SIZE			50u

void MX_USART1_UART_Init(void);
void MX_USART3_UART_Init(void);
void DMA1_Channel2_IRQHandler(void);
void DMA1_Channel4_IRQHandler(void);
void USART1_IRQHandler(void);
void USART3_IRQHandler(void);


typedef struct
{
	uint32_t 	*uwUARTBaud;
	teUARTState eUARTState;
	void		(*UART_TxCpltCb)();
	void		(*UART_RxCplt)();
	void        (*UART_DirectionPinStateChange)(bool pinstate);
	tUARTData*  ptUARTData;
	uint8_t		ubDMATxChannel;


}tUART;


tUART UART[2];

void USART_vInit(teUSARTIface eUART, void* pUART_TxCpltCb, void* pUART_RxCpltCb, uint32_t* ulSpeed, void (*pUART_DirectionPinStateChangeCb)(bool pinstate), tUARTData* pUARTData)
{
	UART[eUART].uwUARTBaud = ulSpeed;
	UART[eUART].UART_RxCplt = pUART_RxCpltCb;
	UART[eUART].UART_TxCpltCb = pUART_TxCpltCb;
	UART[eUART].UART_DirectionPinStateChange = pUART_DirectionPinStateChangeCb;
	UART[eUART].ptUARTData = pUARTData;

	switch(eUART)
	{
		case USART1_IFACE:
			UART[USART1_IFACE].ubDMATxChannel = LL_DMA_CHANNEL_4;
			MX_USART1_UART_Init();
		break;
		case USART3_IFACE:
			UART[USART3_IFACE].ubDMATxChannel = LL_DMA_CHANNEL_2;
			MX_USART3_UART_Init();
		break;
	}

}


void USART_TransmitData(teUSARTIface eUART ,uint8_t* pData, uint8_t length)
{
	if (UART[eUART].UART_DirectionPinStateChange != NULL)
		UART[eUART].UART_DirectionPinStateChange(false); //RTS Out
	LL_DMA_DisableChannel(DMA1, UART[eUART].ubDMATxChannel);
	LL_DMA_SetMemoryAddress(DMA1, UART[eUART].ubDMATxChannel,(uint32_t)pData);
	LL_DMA_SetDataLength(DMA1, UART[eUART].ubDMATxChannel, length);
	LL_DMA_EnableChannel(DMA1, UART[eUART].ubDMATxChannel);
	UART[eUART].eUARTState = UART_STATE_BUSY;


}

void MX_USART1_UART_Init(void)
{
  LL_USART_InitTypeDef USART_InitStruct = {0};

  LL_GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* Peripheral clock enable */
  LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_USART1);
  
  LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_GPIOA);
  /**USART1 GPIO Configuration  
  PA9   ------> USART1_TX
  PA10   ------> USART1_RX 
  */
  GPIO_InitStruct.Pin = UART_PZEM_TX_Pin;
  GPIO_InitStruct.Mode = LL_GPIO_MODE_ALTERNATE;
  GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_HIGH;
  GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
  LL_GPIO_Init(UART_PZEM_TX_GPIO_Port, &GPIO_InitStruct);

  GPIO_InitStruct.Pin = UART_PZEM_RX_Pin;
  GPIO_InitStruct.Mode = LL_GPIO_MODE_FLOATING;
  LL_GPIO_Init(UART_PZEM_RX_GPIO_Port, &GPIO_InitStruct);

  /* USART1 DMA Init */
  
  /* USART1_RX Init */
  LL_DMA_SetDataTransferDirection(DMA1, LL_DMA_CHANNEL_5, LL_DMA_DIRECTION_PERIPH_TO_MEMORY);

  LL_DMA_SetChannelPriorityLevel(DMA1, LL_DMA_CHANNEL_5, LL_DMA_PRIORITY_LOW);

  LL_DMA_SetMode(DMA1, LL_DMA_CHANNEL_5, LL_DMA_MODE_NORMAL);

  LL_DMA_SetPeriphIncMode(DMA1, LL_DMA_CHANNEL_5, LL_DMA_PERIPH_NOINCREMENT);

  LL_DMA_SetMemoryIncMode(DMA1, LL_DMA_CHANNEL_5, LL_DMA_MEMORY_INCREMENT);

  LL_DMA_SetPeriphSize(DMA1, LL_DMA_CHANNEL_5, LL_DMA_PDATAALIGN_BYTE);

  LL_DMA_SetMemorySize(DMA1, LL_DMA_CHANNEL_5, LL_DMA_MDATAALIGN_BYTE);

  /* USART1_TX Init */
  LL_DMA_SetDataTransferDirection(DMA1, LL_DMA_CHANNEL_4, LL_DMA_DIRECTION_MEMORY_TO_PERIPH);

  LL_DMA_SetChannelPriorityLevel(DMA1, LL_DMA_CHANNEL_4, LL_DMA_PRIORITY_LOW);

  LL_DMA_SetMode(DMA1, LL_DMA_CHANNEL_4, LL_DMA_MODE_NORMAL);

  LL_DMA_SetPeriphIncMode(DMA1, LL_DMA_CHANNEL_4, LL_DMA_PERIPH_NOINCREMENT);

  LL_DMA_SetMemoryIncMode(DMA1, LL_DMA_CHANNEL_4, LL_DMA_MEMORY_INCREMENT);

  LL_DMA_SetPeriphSize(DMA1, LL_DMA_CHANNEL_4, LL_DMA_PDATAALIGN_BYTE);

  LL_DMA_SetMemorySize(DMA1, LL_DMA_CHANNEL_4, LL_DMA_MDATAALIGN_BYTE);
  LL_DMA_SetPeriphAddress(DMA1, LL_DMA_CHANNEL_4, LL_USART_DMA_GetRegAddr(USART1));
  LL_DMA_SetPeriphAddress(DMA1, LL_DMA_CHANNEL_5, LL_USART_DMA_GetRegAddr(USART1));
  LL_DMA_SetMemoryAddress(DMA1, LL_DMA_CHANNEL_5, (uint32_t)UART[USART1_IFACE].ptUARTData->pubDatArray);
  LL_DMA_SetDataLength(DMA1, LL_DMA_CHANNEL_5, DMA_PRELOAD_SIZE);


  /* USART1 interrupt Init */
  NVIC_SetPriority(USART1_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(),0, 0));
  NVIC_EnableIRQ(USART1_IRQn);

  USART_InitStruct.BaudRate = *UART[USART1_IFACE].uwUARTBaud;
  USART_InitStruct.DataWidth = LL_USART_DATAWIDTH_8B;
  USART_InitStruct.StopBits = LL_USART_STOPBITS_1;
  USART_InitStruct.Parity = LL_USART_PARITY_NONE;
  USART_InitStruct.TransferDirection = LL_USART_DIRECTION_TX_RX;
  USART_InitStruct.HardwareFlowControl = LL_USART_HWCONTROL_NONE;
  USART_InitStruct.OverSampling = LL_USART_OVERSAMPLING_16;

  LL_USART_Init(USART1, &USART_InitStruct);
  LL_USART_DisableIT_CTS(USART1);
  LL_USART_ConfigAsyncMode(USART1);
  LL_USART_Enable(USART1);

  LL_USART_EnableDMAReq_RX(USART1);
  LL_USART_EnableDMAReq_TX(USART1);
  LL_USART_EnableIT_IDLE(USART1);
  LL_USART_EnableIT_TC(USART1);
  LL_DMA_EnableChannel(DMA1, LL_DMA_CHANNEL_5);

}
/* USART3 init function */

void MX_USART3_UART_Init(void)
{
  LL_USART_InitTypeDef USART_InitStruct = {0};

  LL_GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* Peripheral clock enable */
  LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_USART3);
  
  LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_GPIOB);
  /**USART3 GPIO Configuration  
  PB10   ------> USART3_TX
  PB11   ------> USART3_RX 
  */
  GPIO_InitStruct.Pin = UART_MAIN_TX_Pin;
  GPIO_InitStruct.Mode = LL_GPIO_MODE_ALTERNATE;
  GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_HIGH;
  GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
  LL_GPIO_Init(UART_MAIN_TX_GPIO_Port, &GPIO_InitStruct);

  GPIO_InitStruct.Pin = UART_MAIN_RX_Pin;
  GPIO_InitStruct.Mode = LL_GPIO_MODE_FLOATING;
  LL_GPIO_Init(UART_MAIN_RX_GPIO_Port, &GPIO_InitStruct);

  /* USART3 DMA Init */
  
  /* USART3_RX Init */
  LL_DMA_SetDataTransferDirection(DMA1, LL_DMA_CHANNEL_3, LL_DMA_DIRECTION_PERIPH_TO_MEMORY);

  LL_DMA_SetChannelPriorityLevel(DMA1, LL_DMA_CHANNEL_3, LL_DMA_PRIORITY_LOW);

  LL_DMA_SetMode(DMA1, LL_DMA_CHANNEL_3, LL_DMA_MODE_NORMAL);

  LL_DMA_SetPeriphIncMode(DMA1, LL_DMA_CHANNEL_3, LL_DMA_PERIPH_NOINCREMENT);

  LL_DMA_SetMemoryIncMode(DMA1, LL_DMA_CHANNEL_3, LL_DMA_MEMORY_INCREMENT);

  LL_DMA_SetPeriphSize(DMA1, LL_DMA_CHANNEL_3, LL_DMA_PDATAALIGN_BYTE);

  LL_DMA_SetMemorySize(DMA1, LL_DMA_CHANNEL_3, LL_DMA_MDATAALIGN_BYTE);

  /* USART3_TX Init */
  LL_DMA_SetDataTransferDirection(DMA1, LL_DMA_CHANNEL_2, LL_DMA_DIRECTION_MEMORY_TO_PERIPH);

  LL_DMA_SetChannelPriorityLevel(DMA1, LL_DMA_CHANNEL_2, LL_DMA_PRIORITY_LOW);

  LL_DMA_SetMode(DMA1, LL_DMA_CHANNEL_2, LL_DMA_MODE_NORMAL);

  LL_DMA_SetPeriphIncMode(DMA1, LL_DMA_CHANNEL_2, LL_DMA_PERIPH_NOINCREMENT);

  LL_DMA_SetMemoryIncMode(DMA1, LL_DMA_CHANNEL_2, LL_DMA_MEMORY_INCREMENT);

  LL_DMA_SetPeriphSize(DMA1, LL_DMA_CHANNEL_2, LL_DMA_PDATAALIGN_BYTE);

  LL_DMA_SetMemorySize(DMA1, LL_DMA_CHANNEL_2, LL_DMA_MDATAALIGN_BYTE);

  LL_DMA_SetPeriphAddress(DMA1, LL_DMA_CHANNEL_2, LL_USART_DMA_GetRegAddr(USART3));
  LL_DMA_SetPeriphAddress(DMA1, LL_DMA_CHANNEL_3, LL_USART_DMA_GetRegAddr(USART3));
  LL_DMA_SetMemoryAddress(DMA1, LL_DMA_CHANNEL_3, (uint32_t)UART[USART3_IFACE].ptUARTData->pubDatArray);
  LL_DMA_SetDataLength(DMA1, LL_DMA_CHANNEL_3, DMA_PRELOAD_SIZE);

  /* USART3 interrupt Init */
  NVIC_SetPriority(USART3_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(),0, 0));
  NVIC_EnableIRQ(USART3_IRQn);

  USART_InitStruct.BaudRate = *UART[USART3_IFACE].uwUARTBaud;
  USART_InitStruct.DataWidth = LL_USART_DATAWIDTH_8B;
  USART_InitStruct.StopBits = LL_USART_STOPBITS_1;
  USART_InitStruct.Parity = LL_USART_PARITY_NONE;
  USART_InitStruct.TransferDirection = LL_USART_DIRECTION_TX_RX;
  USART_InitStruct.HardwareFlowControl = LL_USART_HWCONTROL_NONE;
  USART_InitStruct.OverSampling = LL_USART_OVERSAMPLING_16;

  LL_USART_Init(USART3, &USART_InitStruct);
  LL_USART_DisableIT_CTS(USART3);
  LL_USART_ConfigAsyncMode(USART3);
  LL_USART_Enable(USART3);

  LL_USART_EnableDMAReq_RX(USART3);
  LL_USART_EnableDMAReq_TX(USART3);
  LL_USART_EnableIT_IDLE(USART3);
  LL_USART_EnableIT_TC(USART3);
  LL_DMA_EnableChannel(DMA1, LL_DMA_CHANNEL_3);

}

/**
  * @brief This function handles USART1 global interrupt.
  */
void USART1_IRQHandler(void)
{
	if (LL_USART_IsActiveFlag_TC(USART1))
	{
		USART1->DR;
		LL_USART_ClearFlag_TC(USART1);
		if (UART[USART1_IFACE].UART_DirectionPinStateChange != NULL)
		UART[USART1_IFACE].UART_DirectionPinStateChange(true); //RTS In
		UART[USART1_IFACE].eUARTState = UART_STATE_INITIALIZED;
		if (UART[USART1_IFACE].UART_TxCpltCb != NULL)
			UART[USART1_IFACE].UART_TxCpltCb();
	}


    if (LL_USART_IsActiveFlag_IDLE(USART1) != RESET)
    {
    	USART1->DR;
    	LL_USART_ClearFlag_IDLE(USART1);

    	LL_DMA_DisableChannel(DMA1, LL_DMA_CHANNEL_5);
    	*UART[USART1_IFACE].ptUARTData->pubDatLength = DMA_PRELOAD_SIZE - LL_DMA_GetDataLength(DMA1, LL_DMA_CHANNEL_5);
    	*UART[USART1_IFACE].ptUARTData->pbIsNewData = true;
    	if (UART[USART1_IFACE].UART_RxCplt != NULL) UART[USART1_IFACE].UART_RxCplt();
    	LL_DMA_SetDataLength(DMA1, LL_DMA_CHANNEL_5, DMA_PRELOAD_SIZE);
    	LL_DMA_EnableChannel(DMA1, LL_DMA_CHANNEL_5);
    }

}

/**
  * @brief This function handles USART3 global interrupt.
  */
void USART3_IRQHandler(void)
{
	if (LL_USART_IsActiveFlag_TC(USART3) != RESET)
	{
		USART3->DR;
		LL_USART_ClearFlag_TC(USART3);
		if (UART[USART3_IFACE].UART_DirectionPinStateChange != NULL)
		UART[USART3_IFACE].UART_DirectionPinStateChange(true); //RTS In
		UART[USART3_IFACE].eUARTState = UART_STATE_INITIALIZED;
		if (UART[USART3_IFACE].UART_TxCpltCb != NULL)
			UART[USART3_IFACE].UART_TxCpltCb();
	}


    if (LL_USART_IsActiveFlag_IDLE(USART3) != RESET)
    {
    	USART3->DR;
    	LL_USART_ClearFlag_IDLE(USART3);

    	LL_DMA_DisableChannel(DMA1, LL_DMA_CHANNEL_3);
    	*UART[USART3_IFACE].ptUARTData->pubDatLength = DMA_PRELOAD_SIZE - LL_DMA_GetDataLength(DMA1, LL_DMA_CHANNEL_3);
    	*UART[USART3_IFACE].ptUARTData->pbIsNewData = true;
    	if (UART[USART3_IFACE].UART_RxCplt != NULL) UART[USART3_IFACE].UART_RxCplt();
    	LL_DMA_SetDataLength(DMA1, LL_DMA_CHANNEL_3, DMA_PRELOAD_SIZE);
    	LL_DMA_EnableChannel(DMA1, LL_DMA_CHANNEL_3);
    }
}

void DMA1_Channel4_IRQHandler(void)
{
	if (LL_DMA_IsActiveFlag_TC4(DMA1))
	{
		LL_DMA_ClearFlag_TC4(DMA1);
		LL_DMA_DisableChannel(DMA1, LL_DMA_CHANNEL_4);
	}
}

void DMA1_Channel2_IRQHandler(void)
{
	if (LL_DMA_IsActiveFlag_TC2(DMA1))
	{
		LL_DMA_ClearFlag_TC2(DMA1);
		LL_DMA_DisableChannel(DMA1, LL_DMA_CHANNEL_2);
	}
}




/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
