/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.cpp
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include <main.h>
#include "FreeRTOS.h"
#include "task.h"
#include "system_init.h"

#include "PZEM004/PZEM004.h"

//  uint16_t uwAdcBuf[9];

void vBlinky (void *pvParameters)
{
	while(1)
	 {
		LL_GPIO_TogglePin(LED_GPIO_Port, LED_Pin);
		vTaskDelay(500);
	 }
}


/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
	SYSTEM_InitPeriph();
	xPzem004Handle pzem = PZEM004_xInit(PZEM_UART_1);
	PZEM004_xStartValueMonitor(pzem);

//	xPzem004Handle pzem1 = PZEM004_xInit(PZEM_UART_3);
//	PZEM004_xStartValueMonitor(pzem1);
//  LL_DMA_SetMemoryAddress(DMA1, LL_DMA_CHANNEL_1, (uint32_t)uwAdcBuf);
//  LL_DMA_SetPeriphAddress(DMA1, LL_DMA_CHANNEL_1, LL_ADC_DMA_GetRegAddr(ADC1, LL_ADC_DMA_REG_REGULAR_DATA));
//  LL_DMA_SetDataLength(DMA1, LL_DMA_CHANNEL_1, 9);
//  LL_ADC_DisableIT_EOS(ADC1);
//  LL_DMA_EnableChannel(DMA1, LL_DMA_CHANNEL_1);
//  LL_ADC_Enable(ADC1);
//  LL_ADC_REG_StartConversionSWStart(ADC1);


  if(pdTRUE != xTaskCreate(vBlinky,"Blinky", configMINIMAL_STACK_SIZE+256, NULL, tskIDLE_PRIORITY + 1, NULL));
  vTaskStartScheduler();

}


#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
