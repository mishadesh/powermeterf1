/*
 * PZEM004.h
 *
 *  Created on: 12 ���. 2020 �.
 *      Author: Mishadesh
 */

#ifndef PZEM004_PZEM004_H_
#define PZEM004_PZEM004_H_

#include "stdint.h"

typedef void* xPzem004Handle;
typedef enum
{
	PZEM_UART_1 = 0,
	PZEM_UART_3 = 1
}tePZEM_UART;

typedef enum
{
	PZEM_CALIBR_OK =   0,
	PZEM_CALIBR_FAIL = 1,
	PZEM_CALIBR_NOT_DONE = 2,
	PZEM_OBJECT_NOT_CREATED = 3
}tePZEM_CalibrationStatus;


xPzem004Handle PZEM004_xInit(tePZEM_UART ePzemUart);
void PZEM004_xStartValueMonitor(xPzem004Handle pzemHandle);
void PZEM004_xPauseValueMonitor(xPzem004Handle pzemHandle);
void PZEM004_xResumeValueMonitor(xPzem004Handle pzemHandle);
void PZEM004_xStopValueMonitor(xPzem004Handle pzemHandle);
void PZEM004_xDeInit(xPzem004Handle pzemHandle);
tePZEM_CalibrationStatus PZEM004_eGetCalibrationStatus(xPzem004Handle pzemHandle);
void PZEM004_xCalibrate(xPzem004Handle pzemHandle);

/* Getters */

uint16_t PZEM004_uwGetMainVoltage(xPzem004Handle pzemHandle);
uint32_t PZEM004_ulGetCurrentmA(xPzem004Handle pzemHandle);
uint32_t PZEM004_ulGetPowerW(xPzem004Handle pzemHandle);
uint32_t PZEM004_ulGetEnergyWh(xPzem004Handle pzemHandle);
uint16_t PZEM004_uwGetNetFrequencyHz(xPzem004Handle pzemHandle);
uint16_t PZEM004_uwGetPowerFactor(xPzem004Handle pzemHandle);

#endif /* PZEM004_PZEM004_H_ */
