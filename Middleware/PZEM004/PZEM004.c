/*
 * PZEM004.c
 *
 *  Created on: 12 ���. 2020 �.
 *      Author: Mishadesh
 */

#include "PZEM004.h"
#include "Modbus/Inc/Modbus_master.h"
#include "FreeRTOS.h"
#include "portable.h"
#include "task.h"
#include "ErrorHandler/ErrorHandler.h"
#include "string.h"

#define COMMAND_RESET_POWER			0x42


typedef struct
{
	uint16_t uwVoltage;
	uint16_t uwCurrentLow;
	uint16_t uwCurrentHigh;
	uint16_t uwPowerLow;
	uint16_t uwPowerHigh;
	uint16_t uwEnergyLow;
	uint16_t uwEnergyHigh;
	uint16_t uwFrequency;
	uint16_t uwPowerFactor;
	uint16_t uwAlarmStatus;
}PZEM004_Parameters;

typedef struct
{
	uint16_t uwPowerAlarmThreathHold;
	uint16_t uwModbusAddr;
}PZEM004_Settings;

typedef struct
{
	PZEM004_Parameters  pzemParam;
	PZEM004_Settings    pzemSet;
	tePZEM_UART		    eUART;
	xModbusMasterHandle mbHandle;
	TaskHandle_t		xMonitorTask;
	TaskHandle_t		xCalibrTask;
	tePZEM_CalibrationStatus eCalibr;
}PZEM004_Ctrl;

void PZEM004_vUpdateValueReg(void *pvParameters);
void PZEM004_vRunCalibrationTask(void *pvParameters);
void PZEM004_vStoreReceivedData(xPzem004Handle pzemHandle, uint8_t* pData, uint8_t datalen);

xPzem004Handle PZEM004_xInit(tePZEM_UART ePzemUart)
{
	PZEM004_Ctrl *this;
	xPzem004Handle    pzemHandle;
	pzemHandle = pvPortMalloc(sizeof(PZEM004_Ctrl));
	if (pzemHandle == NULL) Error_Handler(E_ERROR_NULL_POINTER);
	this = (PZEM004_Ctrl*)pzemHandle;
	this->eUART = ePzemUart;
	this->eCalibr = PZEM_CALIBR_NOT_DONE;
	this->mbHandle = ModbusMaster_vCreateMaster((teModbusUart)ePzemUart, 9600);

	return pzemHandle;
}

void PZEM004_xStartValueMonitor(xPzem004Handle pzemHandle)
{
	if (pzemHandle != NULL)
	{
		PZEM004_Ctrl *this = (PZEM004_Ctrl*)pzemHandle;
		if (this->xMonitorTask == NULL)
		{
			if(pdTRUE != xTaskCreate(PZEM004_vUpdateValueReg,"PZEM004_vUpdateValueReg", configMINIMAL_STACK_SIZE, pzemHandle, tskIDLE_PRIORITY + 1, &this->xMonitorTask));
		}
	}
}

void PZEM004_xPauseValueMonitor(xPzem004Handle pzemHandle)
{
	if (pzemHandle != NULL)
	{
		PZEM004_Ctrl *this = (PZEM004_Ctrl*)pzemHandle;
		if (this->xMonitorTask != NULL)
		{
			vTaskSuspend(this->xMonitorTask);
		}
	}
}

void PZEM004_xResumeValueMonitor(xPzem004Handle pzemHandle)
{
	if (pzemHandle != NULL)
	{
		PZEM004_Ctrl *this = (PZEM004_Ctrl*)pzemHandle;
		if (this->xMonitorTask != NULL)
		{
			vTaskResume(this->xMonitorTask);
		}
	}
}

void PZEM004_xStopValueMonitor(xPzem004Handle pzemHandle)
{
	if (pzemHandle != NULL)
	{
		PZEM004_Ctrl *this = (PZEM004_Ctrl*)pzemHandle;
		if (this->xMonitorTask != NULL)
		{
			vTaskDelete(this->xMonitorTask);
			this->xMonitorTask = NULL;
		}

	}
}

void PZEM004_xDeInit(xPzem004Handle pzemHandle)
{
	if (pzemHandle != NULL)
	vPortFree(pzemHandle);
}

void PZEM004_xCalibrate(xPzem004Handle pzemHandle)
{
	if (pzemHandle != NULL)
	{
		PZEM004_Ctrl *this = (PZEM004_Ctrl*)pzemHandle;
		PZEM004_xPauseValueMonitor(pzemHandle);
		if (this->xCalibrTask == NULL)
		{
			if(pdTRUE != xTaskCreate(PZEM004_vRunCalibrationTask,"PZEM004_vRunCalibrationTask", configMINIMAL_STACK_SIZE, pzemHandle, tskIDLE_PRIORITY + 2, &this->xCalibrTask));
		}
	}
}

void PZEM004_vRunCalibrationTask(void *pvParameters)
{
	PZEM004_Ctrl *this = (PZEM004_Ctrl*)pvParameters;
	ModbusMaster_GetPayloadBuffer(this->mbHandle);
	uint8_t calibr_req[3] = {0x41, 0x37, 0x21};
	while(1)
	{
		switch (ModbusMaster_GetRequestStatus(this->mbHandle))
		{
			case MB_SLAVE_RESPONCE_ERROR:
			case MB_SLAVE_RESPONCE_TIMEOUT:
			case MB_STATUS_IDLE:
			case MB_STATUS_LINE_NOISE:
				ModbusMaster_RequestCustomData(this->mbHandle, 0xF8, calibr_req, 3, 5000, NULL);
			break;

			case MB_SLAVE_TX_BUSY:
			case MB_SLAVE_REQUEST_SENT:

			break;

			case MB_SLAVE_RESPONCE_RECEIVED:
				ModbusMaster_GetPayloadBuffer(this->mbHandle);
				PZEM004_xResumeValueMonitor(this);
				vTaskDelete(this->xCalibrTask);
				this->xCalibrTask = NULL;
			break;
		}
		vTaskDelay(5);
	}
}

tePZEM_CalibrationStatus PZEM004_eGetCalibrationStatus(xPzem004Handle pzemHandle)
{
	if (pzemHandle!= NULL)
	{
		PZEM004_Ctrl *this = (PZEM004_Ctrl*)pzemHandle;
		return this->eCalibr;
	}
	return PZEM_OBJECT_NOT_CREATED;
}


void PZEM004_vUpdateValueReg(void *pvParameters)
{
	PZEM004_Ctrl *this = (PZEM004_Ctrl*)pvParameters;
	ModbusMaster_GetPayloadBuffer(this->mbHandle);
	while(1)
	{
		switch (ModbusMaster_GetRequestStatus(this->mbHandle))
		{
			case MB_SLAVE_RESPONCE_ERROR:
			case MB_SLAVE_RESPONCE_TIMEOUT:
			case MB_STATUS_IDLE:
			case MB_STATUS_LINE_NOISE:
				ModbusMaster_RequestDataFromRegisters04(this->mbHandle, 0x01, 0x00, 10, 50, NULL);
			break;

			case MB_SLAVE_TX_BUSY:
			case MB_SLAVE_REQUEST_SENT:

			break;

			case MB_SLAVE_RESPONCE_RECEIVED:
				PZEM004_vStoreReceivedData(this, ModbusMaster_GetPayloadBuffer(this->mbHandle), ModbusMaster_GetPayloadLength(this->mbHandle));
			break;
		}
		vTaskDelay(5);
	}
}

void PZEM004_vStoreReceivedData(xPzem004Handle pzemHandle, uint8_t* pData, uint8_t datalen)
{
	if (pzemHandle != NULL && pData != NULL)
	{
		PZEM004_Ctrl *this = (PZEM004_Ctrl*)pzemHandle;
		memcpy(&this->pzemParam, pData, datalen);
		uint8_t *ppzemparam = (uint8_t*)&this->pzemParam;
		for (uint8_t i = 0; i < sizeof(this->pzemParam)-1; i++)
		{
			uint8_t ubTemp = ppzemparam[i];
			ppzemparam[i] = ppzemparam[i+1];
			ppzemparam[i+1] = ubTemp;
		}
	}
}

uint16_t PZEM004_uwGetMainVoltage(xPzem004Handle pzemHandle)
{
	if (pzemHandle != NULL)
	{
		PZEM004_Ctrl *this = (PZEM004_Ctrl*)pzemHandle;
		return this->pzemParam.uwVoltage;
	}
	return 0xFFFF;
}

uint32_t PZEM004_ulGetCurrentmA(xPzem004Handle pzemHandle)
{
	if (pzemHandle != NULL)
	{
		PZEM004_Ctrl *this = (PZEM004_Ctrl*)pzemHandle;
		uint32_t ulCurrent = (this->pzemParam.uwCurrentHigh<<16)|this->pzemParam.uwCurrentLow;
		return ulCurrent;
	}
	return 0xFFFF;
}

uint32_t PZEM004_ulGetPowerW(xPzem004Handle pzemHandle)
{
	if (pzemHandle != NULL)
	{
		PZEM004_Ctrl *this = (PZEM004_Ctrl*)pzemHandle;
		uint32_t ulPower = (this->pzemParam.uwPowerHigh<<16)|this->pzemParam.uwPowerLow;
		return ulPower;
	}
	return 0xFFFF;
}

uint32_t PZEM004_ulGetEnergyWh(xPzem004Handle pzemHandle)
{
	if (pzemHandle != NULL)
	{
		PZEM004_Ctrl *this = (PZEM004_Ctrl*)pzemHandle;
		uint32_t ulEnergy = (this->pzemParam.uwEnergyHigh<<16)|this->pzemParam.uwEnergyLow;
		return ulEnergy;
	}
	return 0xFFFF;
}

uint16_t PZEM004_uwGetNetFrequencyHz(xPzem004Handle pzemHandle)
{
	if (pzemHandle != NULL)
	{
		PZEM004_Ctrl *this = (PZEM004_Ctrl*)pzemHandle;
		return this->pzemParam.uwFrequency;
	}
	return 0xFFFF;
}

uint16_t PZEM004_uwGetPowerFactor(xPzem004Handle pzemHandle)
{
	if (pzemHandle != NULL)
	{
		PZEM004_Ctrl *this = (PZEM004_Ctrl*)pzemHandle;
		return this->pzemParam.uwPowerFactor;
	}
	return 0xFFFF;
}
