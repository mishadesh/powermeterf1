/*
 * ErrorHandler.h
 *
 *  Created on: 14 ���. 2020 �.
 *      Author: Mishadesh
 */

#ifndef ERRORHANDLER_H_
#define ERRORHANDLER_H_

typedef enum
{
	E_ERROR_NULL_POINTER = 0,
	E_ERROR_UNEXPECTED_VALUE = 1,
	E_ERROR_SYSCLOCK 		 = 2
}eErrorList;

void Error_Handler(eErrorList eError, ...);

#endif /* ERRORHANDLER_H_ */
