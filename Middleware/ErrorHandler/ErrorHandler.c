/*
 * ErrorHandler.c
 *
 *  Created on: 14 ���. 2020 �.
 *      Author: Mishadesh
 */

#include "ErrorHandler.h"

#ifdef USE_RTOS
#include "FreeRTOS.h"
#include "task.h"
#include "portable.h"
#endif
/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(eErrorList eError, ...)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
	xPortGetFreeHeapSize();
	xPortGetMinimumEverFreeHeapSize();
	while(1);
  /* USER CODE END Error_Handler_Debug */
}
