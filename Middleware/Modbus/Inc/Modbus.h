/*
 * Modbus.h
 *
 *  Created on: 19 ��� 2019 �.
 *      Author: Mishadesh-x12
 */

#ifndef PROTOCOLS_INC_MODBUS_H_
#define PROTOCOLS_INC_MODBUS_H_

#include <System_config.h>

#define DEVICE_ADDR 0
#define CMD			1
#define ADDR_HIGH   2
#define ADDR_LOW    3
#define DATA_HIGH   4
#define DATA_LOW    5

#define MODBUS_CMD_03 0x03
#define MODBUS_CMD_06 0x06


#define EXCEPTION_ILLEGAL_FUNCTION 		0x01
#define EXCEPTION_ILLEGAL_ADDRESS 		0x02
#define EXCEPTION_ILLEGAL_DATA 			0x03
#define EXCEPTION_SLAVE_FAILURE 		0x04
#define EXCEPTION_MASK					0x80


typedef enum
{
	MODBUS_OK = 0,
	MODBUS_ERROR_SEND,
	MODBUS_ERROR_CRC,
	MODBUS_ERROR_PARSE_DATA,
	MODBUS_ERROR_WRONG_ADDR,
	MODBUS_ERROR_WRONG_DATA,
	MODBUS_ERROR_UNSUPPORTED_FUNCTION,
	MODBUS_ERROR_SLAVE_FAILURE
}Modbus_Error;


#endif /* PROTOCOLS_INC_MODBUS_H_ */
