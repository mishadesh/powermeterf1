/*
 * Modbus_master.h
 *
 *  Created on: 19 ��� 2019 �.
 *      Author: Mishadesh-x12
 */

#ifndef PROTOCOLS_INC_MODBUS_MASTER_H_
#define PROTOCOLS_INC_MODBUS_MASTER_H_

#include "stdint.h"
#include "stdbool.h"

typedef enum
{
	MODBUS_UART_1 = 0,
	MODBUS_UART_3 = 1
}teModbusUart;

typedef enum
{
	MB_SLAVE_RESPONCE_TIMEOUT  = 0,
	MB_SLAVE_RESPONCE_ERROR    = 1,
	MB_SLAVE_RESPONCE_RECEIVED = 2,
	MB_SLAVE_REQUEST_SENT      = 3,
	MB_SLAVE_TX_BUSY		   = 4,
	MB_STATUS_IDLE			   = 5,
	MB_STATUS_LINE_NOISE	   = 6
}teModbusRequestStatus;

typedef void* xModbusMasterHandle;
typedef void (*MbMasterOnReceiveCallback)(void* pvoid, uint8_t* pData, uint8_t ubDataLen);

xModbusMasterHandle 	ModbusMaster_vCreateMaster(teModbusUart _eUART_Iface, uint32_t _ulBaudrate);
void 					ModbusMaster_vRemoveMaster(xModbusMasterHandle this);
bool 					ModbusMaster_RequestDataFromRegisters04(xModbusMasterHandle pModbusMasterHandle, uint16_t uwSlaveAddr,
							uint16_t uwRegAddr, uint16_t uwRegCount, uint16_t _ubReqTimeout, MbMasterOnReceiveCallback _pRecvCb);
bool 					ModbusMaster_RequestDataFromRegisters03(xModbusMasterHandle pModbusMasterHandle, uint16_t uwSlaveAddr,
							uint16_t uwRegAddr, uint16_t uwRegCount, uint16_t _ubReqTimeout, MbMasterOnReceiveCallback _pRecvCb);
bool 					ModbusMaster_RequestCustomData(xModbusMasterHandle pModbusMasterHandle, uint16_t uwSlaveAddr, uint8_t* pBufCustom,
							uint8_t ubBufLen, uint16_t ReqTimeout, MbMasterOnReceiveCallback _pRecvCb);
uint8_t 				ModbusMaster_GetPayloadLength(xModbusMasterHandle pModbusMasterHandle);
uint8_t* 				ModbusMaster_GetPayloadBuffer(xModbusMasterHandle pModbusMasterHandle);
teModbusRequestStatus 	ModbusMaster_GetRequestStatus(xModbusMasterHandle pModbusMasterHandle);


#endif /* PROTOCOLS_INC_MODBUS_MASTER_H_ */
