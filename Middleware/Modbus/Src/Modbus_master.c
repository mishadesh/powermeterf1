/*
 * Modbus_master.c
 *
 *  Created on: 19 ��� 2019 �.
 *      Author: Mishadesh-x12
 */
#include "Modbus/Inc/Modbus_master.h"
#include "../Inc/Crc16.h"

#include "FreeRTOS.h"
#include "task.h"
#include "portable.h"

#include "usart.h"
#include "string.h"

#include "ErrorHandler/ErrorHandler.h"


#define EXCEPTON_MASK		0x80
#define UW_GET_HIGH(word)  (uint8_t)((word>>8)&0x00ff)
#define UW_GET_LOW(word)   (uint8_t)(word &0x00ff)

static void ModbusMaster_vProcessResponce (void *pvParameters);

typedef struct	{
	uint32_t 	ulBaudrate;
	bool		bIsRequestSent;
	uint16_t 	uwLastReqSlaveId;
	tUARTData   sUARTData;
	uint8_t		ubRequestBuffer[50];
	uint8_t     ubResponceBuffer[50];
	bool		bIsDataReceived;
	uint8_t     ubRespLen;
	teUSARTIface eUART_Iface;
	uint16_t     ubResponceToMs;
	uint16_t     ubReqTimeout;
	teModbusRequestStatus eReqStatus;
	MbMasterOnReceiveCallback pRecvCb;

	}tModbusMasterInt;

xModbusMasterHandle ModbusMaster_vCreateMaster(teModbusUart _eUART_Iface, uint32_t _ulBaudrate)
{
	tModbusMasterInt *this;
	xModbusMasterHandle pMbHandle;
	if (_ulBaudrate==0) Error_Handler(E_ERROR_UNEXPECTED_VALUE);

	pMbHandle = pvPortMalloc(sizeof(tModbusMasterInt));
	if (pMbHandle == NULL) Error_Handler(E_ERROR_NULL_POINTER);
	this = (tModbusMasterInt*)pMbHandle;
	this->ulBaudrate = _ulBaudrate;
	this->eUART_Iface = (teUSARTIface)_eUART_Iface;
	this->sUARTData.pubDatArray = this->ubResponceBuffer;
	this->sUARTData.pubDatLength = &this->ubRespLen;
	this->sUARTData.pbIsNewData = &this->bIsDataReceived;
	USART_vInit(this->eUART_Iface, NULL, NULL, &this->ulBaudrate, NULL, &this->sUARTData);
	if(pdTRUE != xTaskCreate(ModbusMaster_vProcessResponce,"MODBUS_vProcessResponce", configMINIMAL_STACK_SIZE+64, pMbHandle, tskIDLE_PRIORITY + 1, NULL));
	return pMbHandle;
}

void ModbusMaster_vRemoveMaster(xModbusMasterHandle this)
{
	vPortFree(this);
}

bool ModbusMaster_RequestDataFromRegisters04(xModbusMasterHandle pModbusMasterHandle, uint16_t uwSlaveAddr, uint16_t uwRegAddr,
												uint16_t uwRegCount, uint16_t _ubReqTimeout, MbMasterOnReceiveCallback _pRecvCb)
{
	tModbusMasterInt* this = pModbusMasterHandle;
	uint16_t uwCRC = 0;
	if (!this->bIsRequestSent)
	{
		this->ubReqTimeout	   = _ubReqTimeout;
		this->uwLastReqSlaveId   = uwSlaveAddr;
		this->pRecvCb  = _pRecvCb;
		this->ubRequestBuffer[0] = uwSlaveAddr;
		this->ubRequestBuffer[1] = 0x04;
		this->ubRequestBuffer[2] = UW_GET_HIGH(uwRegAddr);
		this->ubRequestBuffer[3] = UW_GET_LOW(uwRegAddr);
		this->ubRequestBuffer[4] = UW_GET_HIGH(uwRegCount);
		this->ubRequestBuffer[5] = UW_GET_LOW(uwRegCount);
		uwCRC = Modbus_CRC16(this->ubRequestBuffer, 6);
		this->ubRequestBuffer[6] = UW_GET_LOW(uwCRC);
		this->ubRequestBuffer[7] = UW_GET_HIGH(uwCRC);
		USART_TransmitData(this->eUART_Iface, this->ubRequestBuffer, 8);
		this->bIsRequestSent = true;
		this->eReqStatus = MB_SLAVE_REQUEST_SENT;
		return true;
	}
	else
	{
		this->eReqStatus = MB_SLAVE_TX_BUSY;
		return false;
	}
}

bool ModbusMaster_RequestDataFromRegisters03(xModbusMasterHandle pModbusMasterHandle, uint16_t uwSlaveAddr, uint16_t uwRegAddr,
												uint16_t uwRegCount, uint16_t ReqTimeout, MbMasterOnReceiveCallback _pRecvCb)
{
	tModbusMasterInt* this = pModbusMasterHandle;
	uint16_t uwCRC = 0;
	if (!this->bIsRequestSent)
	{
		this->uwLastReqSlaveId	 = uwSlaveAddr;
		this->pRecvCb = _pRecvCb;
		this->ubRequestBuffer[0] = uwSlaveAddr;
		this->ubRequestBuffer[1] = 0x03;
		this->ubRequestBuffer[2] = UW_GET_LOW(uwRegAddr);
		this->ubRequestBuffer[3] = UW_GET_HIGH(uwRegAddr);
		this->ubRequestBuffer[4] = UW_GET_LOW(uwRegCount);
		this->ubRequestBuffer[5] = UW_GET_HIGH(uwRegCount);
		uwCRC = Modbus_CRC16(this->ubRequestBuffer, 6);
		this->ubRequestBuffer[6] = UW_GET_LOW(uwCRC);
		this->ubRequestBuffer[7] = UW_GET_HIGH(uwCRC);
		USART_TransmitData(this->eUART_Iface, this->ubRequestBuffer, 8);
		this->bIsRequestSent = true;
		this->eReqStatus = MB_SLAVE_REQUEST_SENT;
		return true;
	}
	else
	{
		this->eReqStatus = MB_SLAVE_TX_BUSY;
		return false;
	}
}

bool ModbusMaster_RequestCustomData(xModbusMasterHandle pModbusMasterHandle, uint16_t uwSlaveAddr, uint8_t* pBufCustom,
									uint8_t ubBufLen, uint16_t ReqTimeout, MbMasterOnReceiveCallback _pRecvCb)
{
	tModbusMasterInt* this = pModbusMasterHandle;
	uint16_t uwCRC = 0;
	if (!this->bIsRequestSent)
	{
		this->uwLastReqSlaveId	 = uwSlaveAddr;
		this->pRecvCb = _pRecvCb;
		this->ubRequestBuffer[0] = uwSlaveAddr;
		memcpy(&this->ubRequestBuffer[1], pBufCustom, ubBufLen);
		uwCRC = Modbus_CRC16(this->ubRequestBuffer, ubBufLen+1);
		this->ubRequestBuffer[ubBufLen+1] = UW_GET_LOW(uwCRC);
		this->ubRequestBuffer[ubBufLen+2] = UW_GET_HIGH(uwCRC);
		USART_TransmitData(this->eUART_Iface, this->ubRequestBuffer, ubBufLen+3);
		this->bIsRequestSent = true;
		this->eReqStatus = MB_SLAVE_REQUEST_SENT;
		return true;
	}
	this->eReqStatus = MB_SLAVE_TX_BUSY;
	return false;
}

static void ModbusMaster_vProcessResponce (void *pvParameters)
{
	tModbusMasterInt *this = (tModbusMasterInt*)pvParameters;
	while(1)
	{
		if (this->bIsRequestSent)
		{
			this->ubResponceToMs++;
			if (this->bIsDataReceived)
			{
				if (((uint16_t)this->ubResponceBuffer[this->ubRespLen-2]|(this->ubResponceBuffer[this->ubRespLen-1]<<8))
							== Modbus_CRC16(this->ubResponceBuffer, this->ubRespLen-2))
				{
					if (this->ubResponceBuffer[0] == this->uwLastReqSlaveId)
					{
						if (this->ubResponceBuffer[1] == this->ubRequestBuffer[1] )
						{
							if (this->pRecvCb != NULL)
							{
								this->pRecvCb(pvParameters ,&this->ubResponceBuffer[3], this->ubResponceBuffer[2]);
								this->eReqStatus = MB_STATUS_IDLE;
							}
							else
							this->eReqStatus = MB_SLAVE_RESPONCE_RECEIVED;
						}
						else if(this->ubResponceBuffer[1] == (this->ubRequestBuffer[1]|EXCEPTON_MASK))
						{
							this->eReqStatus = MB_SLAVE_RESPONCE_ERROR;
						}
					}
				}
				this->bIsDataReceived = false;
				this->bIsRequestSent = false;
				this->ubResponceToMs = 0;
				//todo add timeout for responce
			}
			else if (this->ubResponceToMs > this->ubReqTimeout)
			{
				this->bIsRequestSent = false;
				this->ubResponceToMs = 0;
				this->eReqStatus = MB_SLAVE_RESPONCE_TIMEOUT;
			}
		}
		else if (this->bIsDataReceived)
		{
			this->bIsDataReceived = false;
			this->eReqStatus = MB_STATUS_LINE_NOISE;
		}
		vTaskDelay(5);
	}
}

uint8_t ModbusMaster_GetPayloadLength(xModbusMasterHandle pModbusMasterHandle)
{
	tModbusMasterInt* this = pModbusMasterHandle;
	return this->ubRespLen;
}

uint8_t* ModbusMaster_GetPayloadBuffer(xModbusMasterHandle pModbusMasterHandle)
{
	tModbusMasterInt* this = pModbusMasterHandle;
	this->eReqStatus = MB_STATUS_IDLE;
	return &this->ubResponceBuffer[3];
}

teModbusRequestStatus ModbusMaster_GetRequestStatus(xModbusMasterHandle pModbusMasterHandle)
{
	tModbusMasterInt* this = pModbusMasterHandle;
	return this->eReqStatus;
}
